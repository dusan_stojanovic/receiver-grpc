package main

import (
	"log"
	"receiver-grpc/grpcserver"
)

func main() {
	log.Println("Starting Receiver Service")
	grpcserver.StartGrpcServer()
}
