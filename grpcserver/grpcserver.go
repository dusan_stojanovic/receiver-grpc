package grpcserver

import (
	"context"
	"log"
	"net"
	"receiver-grpc/messages"

	"google.golang.org/grpc"
)

func StartGrpcServer() {
	listener, err := net.Listen("tcp", ":8081")
	if err != nil {
		log.Fatalf("gRPC Server starting error: %v", err)
	}

	grpcServer := grpc.NewServer()
	messages.RegisterMessagesGrpcServer(grpcServer, &messagesGrpcServer{})
	grpcServer.Serve(listener)
}

type messagesGrpcServer struct {
	messages.UnimplementedMessagesGrpcServer
}

func (ms *messagesGrpcServer) CreateMessage(ctx context.Context, request *messages.MessagesRequest) (*messages.MessagesResponse, error) {
	log.Println(request.Content)
	return &messages.MessagesResponse{
		Status: "OK",
	}, nil
}
