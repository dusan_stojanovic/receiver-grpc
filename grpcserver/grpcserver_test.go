package grpcserver

import (
	"context"
	"log"
	"net"
	"receiver-grpc/messages"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
)

func TestCreateMessage(t *testing.T) {
	ctx := context.Background()
	client, closer := initTest(ctx)

	defer closer()

	request := &messages.MessagesRequest{
		Content: "Hello World!!!",
	}

	response, err := client.CreateMessage(ctx, request)
	require.NoError(t, err)
	assert.Equal(t, "OK", response.Status)
}

func initTest(ctx context.Context) (messages.MessagesGrpcClient, func()) {
	listener := bufconn.Listen(101024 * 1024)
	testServer := grpc.NewServer()
	messages.RegisterMessagesGrpcServer(testServer, &messagesGrpcServer{})
	go testServer.Serve(listener)

	optsCtx := grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	})
	optsCred := grpc.WithTransportCredentials(insecure.NewCredentials())

	conn, _ := grpc.DialContext(ctx, "", optsCtx, optsCred)
	client := messages.NewMessagesGrpcClient(conn)

	closer := func() {
		err := listener.Close()
		if err != nil {
			log.Printf("Closing listener error: %v", err)
		}
		testServer.Stop()
	}

	return client, closer
}
